#!/bin/bash
mkdir -p /etc/yum.repos.d/repo_bak/
mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/repo_bak/
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.myhuaweicloud.com/repo/CentOS-Base-7.repo
yum -y install expect
/usr/bin/expect <<EOF
spawn sh /tmp/LinuxVMDataDiskAutoInitialize.sh
expect "Step 3: Please choose the dis(e.g: /dev/vdb and q to quit):"
send "/dev/vdb\n"
expect "Please enter a location to mount (e.g: /mnt/data):"
send "/data_disk\n"
expect eof
EOF
yum install -y postgresql-server
mkdir /data_disk/pgsql_data
chown -R postgres.postgres /data_disk/pgsql_data
sed -i "s|/var/lib/pgsql/data|/data_disk/pgsql_data|" /usr/lib/systemd/system/postgresql.service
/usr/bin/postgresql-setup initdb
mkdir /data_disk/pgsql_data/archivelog/
systemctl enable postgresql
systemctl start postgresql
su - postgres -c psql <<EOF
CREATE ROLE dbar SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN PASSWORD '$1';
\q
exit
EOF
echo "host all all 172.16.0.0/16 md5" >> /data_disk/pgsql_data/pg_hba.conf
echo "host replication dbar 172.16.0.11/32 md5" >> /data_disk/pgsql_data/pg_hba.conf
echo "host replication dbar 172.16.0.12/32 md5" >> /data_disk/pgsql_data/pg_hba.conf
echo "wal_level = hot_standby" >> /data_disk/pgsql_data/postgresql.conf
echo "max_wal_senders= 6" >> /data_disk/pgsql_data/postgresql.conf
echo "replication_timeout = 60s" >> /data_disk/pgsql_data/postgresql.conf
echo "max_connections = 512" >> /data_disk/pgsql_data/postgresql.conf
echo "archive_command = 'cp %p /data_disk/pgsql_data/archivelog/%f'" >> /data_disk/pgsql_data/postgresql.conf
echo "wal_keep_segments = 10240" >> /data_disk/pgsql_data/postgresql.conf
echo "archive_mode = on" >> /data_disk/pgsql_data/postgresql.conf
echo "listen_addresses = '172.16.0.10'" >> /data_disk/pgsql_data/postgresql.conf
systemctl restart postgresql
rm -rf /tmp/LinuxVMDataDiskAutoInitialize.sh