[TOC]

**解决方案介绍**
===============
该解决方案可以帮助您在华为云弹性云服务器上轻松搭建基于流复制的高可用PostgreSQL集群。PostgreSQL是一个开源对象关系型数据库管理系统，并侧重于可扩展性和标准的符合性。流复制是PostgreSQL一种强大的高可用技术，通过传输WAL日志保障主备库的一致性。

解决方案实践详情页面：[https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-ha-postgresql.html](https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-ha-postgresql.html)


**架构图**
---------------
![方案架构](./document/rapid-deployment-of-ha-postgresql.png)

**架构描述**
---------------
该解决方案会部署如下资源：

1、创建3台弹性云服务器部署在不同的可用区，分别用于搭建PostgreSQL集群的主节点、备节点。

2、创建安全组，通过配置安全组规则，为弹性云服务器提供安全防护。此外您还可以使用云监控服务来监测弹性云服务器运行状态；通过购买云备份服务，对弹性云服务器进行数据备份。

**组织结构**
---------------

``` lua
huaweicloud-solution-rapid-deployment-of-ha-postgresql
├── rapid-deployment-of-ha-postgresql.tf.json -- 资源编排模板
├── userdata
	├── install_primary_db.sh  -- 主库脚本配置文件
	├── install_secondary_db01.sh  -- 从库脚本配置文件
	├── install_secondary_db02.sh  -- 从库脚本配置文件
```
**开始使用**
---------------
**PostgreSQL数据库登录**

1.登录[ECS弹性云服务器](https://console.huaweicloud.com/ecm/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)控制平台，选择创建的后缀为primary的弹性云服务器，单击远程登录，进入Linux弹性云服务器。

图1 登录ECS云服务器控制平台

![登录ECS云服务器控制平台](./document/readme-image-001.png)

图2 登录ECS云服务器控制平台

![登录ECS云服务器控制平台](./document/readme-image-002.png)

2.在Linux弹性云服务中输入账号和密码后回车。

图3 登录ECS弹性云服务器

![登录ECS弹性云服务器](./document/readme-image-003.png)

3.执行su - postgres，切换到postgres用户（安装好生成默认的用户）。输入命令psql，登录数据库。数据库安装路径默认为/data_disk/pgsql_data。

图4 登录PostgreSQL数据库

![登录PostgreSQL数据库](./document/readme-image-004.png)

**修改PostgreSQL数据库默认用户postgres的密码**

1.执行su - postgres，切换到postgres用户（安装好生成默认的用户）。输入命令psql，登录数据库。

图5 登录PostgreSQL数据库

![登录PostgreSQL数据库](./document/readme-image-013.png)

2.执行以下命令，修改登录PostgreSQL密码

	ALTER USER postgres WITH PASSWORD 'postgres';

图6 修改登录PostgreSQL数据库密码

![修改登录PostgreSQL密码](./document/readme-image-014.png)

3.执行以下命令，修改Linux系统postgres用户密码

	删除postgres用户密码
	sudo passwd -d postgres
	
	设置用户postgres的密码
	sudo -u postgres passwd

图7 修改postgres用户密码

![修改postgres用户密码](./document/readme-image-015.png)

**验证PostgreSQL部署成功**

1.单击该方案堆栈后的“输出”,根据回显登录[ECS弹性云服务器](https://console.huaweicloud.com/ecm/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)控制平台验证。

图8 输出回显命令

![输出回显命令](./document/readme-image-005.png)

2.登录[ECS弹性云服务器](https://console.huaweicloud.com/ecm/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)控制平台，选择创建的后缀为primary的弹性云服务器，单击远程登录，进入Linux弹性云服务器。

图9 登录ECS云服务器控制平台

![登录ECS云服务器控制平台](./document/readme-image-001.png)

图10 登录ECS云服务器控制平台

![登录Linux弹性云服务器](./document/readme-image-002.png)

3.输入命令ps aux |grep sender，在主节点中可查看到sender进程。

图11 主节点查看sender进程

![主节点查看sender进程](./document/readme-image-006.png)

4.登录[ECS弹性云服务器](https://console.huaweicloud.com/ecm/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)控制平台，选择创建的后缀为secondary01的弹性云服务器，单击远程登录，进入Linux弹性云服务器。

图12 登录ECS云服务器控制平台

![登录ECS云服务器控制平台](./document/readme-image-007.png)

图13 登录ECS云服务器控制平台

![登录Linux弹性云服务器](./document/readme-image-002.png)

5.输入命令ps aux | grep receiver，从节点中可查看到receiver进程

图14 从节点查看receiver进程

![从节点查看receiver进程](./document/readme-image-008.png)

6.执行以下命令，主库中可查看到从库状态。
<br>

	su - postgres
	psql
	replication=# select * from pg_stat_replication;

图15 查看从库状态

![查看从库状态](./document/readme-image-009.png)

7.在主库建一个库，验证备库是否可以同步。

1）在主库执行以下命令

	postgres=# create database testdb;
	postgres=# \l

图16 建库

![建库](./document/readme-image-010.png)

图17 查库

![查库](./document/readme-image-011.png)

2）在备库执行以下命令查看

	postgres=# \l

图18 查库

![查库](./document/readme-image-012.png)